package com.believeapps.coctail.screens.ingredientlisting;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.believeapps.coctail.R;
import com.believeapps.coctail.screens.common.RootAdapter;
import com.believeapps.coctail.data.model.Ingredient;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IngredientAdapter extends RootAdapter<Ingredient, IngredientAdapter.ViewHolder> {


    public IngredientAdapter(Context context) {
        super(context);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_name_tv)
        TextView ingredientName;

        @BindView(R.id.grid_item_img)
        ImageView ingredientImg;

        public Ingredient ingredient;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    protected ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.grid_item, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(IngredientAdapter.ViewHolder holder, Ingredient ingredient) {
        holder.ingredient = ingredient;
        holder.ingredientName.setText(holder.ingredient.getName());
        Picasso.with(context).load(holder.ingredient.getMediumImagePath())
                .into(holder.ingredientImg);
    }


}



