package com.believeapps.coctail.screens.drinklisting;


import dagger.Subcomponent;

@FragmentScope
@Subcomponent(modules = {DrinkListingFragModule.class})
public interface DrinkListingComponent {

    void inject(DrinkListingFragment target);
}
