package com.believeapps.coctail.screens;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MenuItem;

import com.believeapps.coctail.screens.common.DrawerActivity;
import com.believeapps.coctail.R;
import com.believeapps.coctail.screens.populardrinks.PopularDrinksFragment;
import com.believeapps.coctail.screens.ingredientlisting.IngredientListFragment;

public class MainActivity extends DrawerActivity {


    //log tag
    private static final String TAG = "MainActivity";

    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_POPULAR = "popular";
    private static final String TAG_INGREDIENT_LIST = "ingredientlist";

    //current active fragment
    public static String CURRENT_TAG = TAG_POPULAR;

    private Handler handler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupNavigationView();
        handler = new Handler();


        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_POPULAR;
            loadFragment();
        }
    }


    private void setupNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_popular:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_POPULAR;
                        break;
                    case R.id.nav_by_ingredient:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_INGREDIENT_LIST;
                        break;
                    default:
                        Log.d(TAG, "onNavigationItemSelected: diffrent item selected");
                }

                if (item.isChecked()) {
                    item.setChecked(false);
                } else {
                    item.setChecked(true);
                }
                item.setChecked(true);

                loadFragment();

                return true;
            }
        });
    }


    private void loadFragment() {

        selectNavMenu();

        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawerLayout.closeDrawers();
            return;
        }

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.container_fragment, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            handler.post(mPendingRunnable);
        }

        drawerLayout.closeDrawers();

    }

    private Fragment getFragment() {
        switch (navItemIndex) {
            case 0:
                return PopularDrinksFragment.newInstance();

            case 1:
                return new IngredientListFragment();

            default:
                return PopularDrinksFragment.newInstance();
        }
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

}
