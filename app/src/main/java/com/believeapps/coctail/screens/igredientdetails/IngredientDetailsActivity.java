package com.believeapps.coctail.screens.igredientdetails;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.believeapps.coctail.R;
import com.believeapps.coctail.data.model.Ingredient;


public class IngredientDetailsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredient_details);


        if (savedInstanceState == null) {

            Bundle extras = getIntent().getExtras();
            if (extras != null && extras.containsKey(Ingredient.class.getSimpleName())) {

                Ingredient ingredient = extras.getParcelable(Ingredient.class.getSimpleName());
                IngredientDetailsFragment fragment = IngredientDetailsFragment.newInstance(ingredient);

                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.ingredient_details_container, fragment)
                        .commit();
            }
        }
    }

    public static Intent newIntent(Context packageContext, Ingredient ingredient) {
        Intent intent = new Intent(packageContext, IngredientDetailsActivity.class);
        Bundle extras = new Bundle();
        extras.putParcelable(Ingredient.class.getSimpleName(), ingredient);
        intent.putExtras(extras);
        return intent;
    }
}

