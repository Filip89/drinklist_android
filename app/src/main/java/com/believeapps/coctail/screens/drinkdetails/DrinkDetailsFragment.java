package com.believeapps.coctail.screens.drinkdetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.believeapps.coctail.R;
import com.believeapps.coctail.screens.common.RootAdapter;
import com.believeapps.coctail.data.model.Drink;
import com.believeapps.coctail.data.model.DrinkDetail;
import com.believeapps.coctail.data.model.DrinkIngredient;
import com.believeapps.coctail.data.model.Ingredient;
import com.believeapps.coctail.screens.igredientdetails.IngredientDetailsActivity;
import com.believeapps.coctail.root.BaseApplication;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrinkDetailsFragment extends Fragment implements DrinkDetailsContract.View {

    private static final String TAG = "DrinkDetailsFragment";


    @Inject
    DrinkDetailsContract.Presenter presenter;

    @BindView(R.id.detail_drink_img)
    ImageView imageView;

    @BindView(R.id.drink_detail_instructions_tx)
    TextView instructionsTX;

    @BindView(R.id.drink_details_recycler)
    RecyclerView recycler;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @BindView(R.id.drink_details_title_tx)
    TextView titleTX;

    DrinkDetail drinkDetail;

    RootAdapter<DrinkIngredient, DrinkIngredientAdapter.ViewHolder> adapter;

    public DrinkDetailsFragment() {}


    public static DrinkDetailsFragment getInstance(@NonNull Drink drink) {

        Bundle args = new Bundle();
        args.putParcelable(Drink.class.getSimpleName(), drink);
        DrinkDetailsFragment fragment = new DrinkDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.detail_drink_fragment, container, false);
        ButterKnife.bind(this, root);
        setupToolbar();
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((BaseApplication) getActivity().getApplication()).getAppComponent()
                .plus(new DrinkDetailsModule())
                .inject(this);
        presenter.setView(this);
        initRecycler();

        if (savedInstanceState == null) {
            Drink drink = getArguments().getParcelable(Drink.class.getSimpleName());
            if (drink != null) {
                presenter.loadDrinkDetails(drink.getIdDrink());
            }
        } else {
            showDrinkDetail(savedInstanceState.getParcelable(DrinkDetail.class.getSimpleName()));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(DrinkDetail.class.getSimpleName(), drinkDetail);
        super.onSaveInstanceState(outState);
    }


    @Override
    public void showDrinkDetail(DrinkDetail drinkDetail) {
        this.drinkDetail = drinkDetail;
        Picasso.with(getContext()).load(drinkDetail.getStrDrinkThumb()).into(imageView);
        titleTX.setText(drinkDetail.getStrDrink());
        instructionsTX.setText(drinkDetail.getStrInstructions());
        adapter.swap(drinkDetail.getDrinkIngredients());

    }

    void setupToolbar() {

        collapsingToolbar.setContentScrimColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        collapsingToolbar.setTitle(getString(R.string.DrinkDetailsToolbarTitle));
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedToolbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedToolbar);
        collapsingToolbar.setTitleEnabled(true);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void navigateToIngredientDetails(Ingredient ingredient) {
        startActivity(IngredientDetailsActivity.newIntent(getContext(), ingredient));
    }

    private void initRecycler() {
        adapter = new DrinkIngredientAdapter(getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setAutoMeasureEnabled(true);
        adapter.setOnItemClickListener(this::navigateToIngredientDetails);
        recycler.setLayoutManager(layoutManager);
        recycler.setNestedScrollingEnabled(false);
        recycler.setAdapter(adapter);
    }

}
