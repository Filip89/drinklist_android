package com.believeapps.coctail.screens.drinklisting;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.believeapps.coctail.R;
import com.believeapps.coctail.screens.common.RootAdapter;
import com.believeapps.coctail.data.model.Drink;
import com.squareup.picasso.Picasso;


import butterknife.BindView;
import butterknife.ButterKnife;

public class DrinkAdapter extends RootAdapter<Drink, DrinkAdapter.ViewHolder> {


    public DrinkAdapter(Context context) {
        super(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_name_tv)
        TextView drinkName;

        @BindView(R.id.grid_item_img)
        ImageView drinkImg;

        public Drink drink;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    @Override
    protected ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.grid_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, Drink drink) {
        holder.drink = drink;
        holder.drinkName.setText(holder.drink.getStrDrink());
        Picasso.with(context).load(holder.drink.getStrDrinkThumb())
                .into(holder.drinkImg);
    }

}
