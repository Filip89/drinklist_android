package com.believeapps.coctail.screens.igredientdetails;

import com.believeapps.coctail.data.DataSource;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class IngredientDetailsPresenter implements IngredientDetailsContract.Presenter {


    private IngredientDetailsContract.View view;

    private final DataSource dataSource;

    public IngredientDetailsPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void setView(IngredientDetailsContract.View view) {
        this.view = view;
    }

    @Override
    public void loadData(String ingredientName) {
        dataSource.getIngredientDetail(ingredientName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        view::showData
                );
    }
}
