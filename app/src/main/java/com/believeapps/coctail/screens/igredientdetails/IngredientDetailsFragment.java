package com.believeapps.coctail.screens.igredientdetails;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.believeapps.coctail.R;
import com.believeapps.coctail.data.model.Ingredient;
import com.believeapps.coctail.data.model.IngredientDetail;
import com.believeapps.coctail.screens.drinklisting.DrinkListingFragment;
import com.believeapps.coctail.root.BaseApplication;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class IngredientDetailsFragment extends Fragment implements IngredientDetailsContract.View {

    private static final String TAG = "IngredientDetailsFragme";

    private static final String INGREDIENT = "ingredient";


    private Ingredient ingredient;


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.ingredient_detail_img)
    ImageView ingredientImg;

//    @BindView(R.id.ingredient_details_title_tx)
//    TextView ingredientNameTX;

//    @BindView(R.id.ingredient_detail_tx)
//    TextView descriptionTX;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @Inject
    IngredientDetailsContract.Presenter presenter;


    public IngredientDetailsFragment() {
        // Required empty public constructor
    }


    public static IngredientDetailsFragment newInstance(Ingredient ingredient) {
        IngredientDetailsFragment fragment = new IngredientDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(INGREDIENT, ingredient);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ingredient = getArguments().getParcelable(INGREDIENT);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_ingredient_details, container, false);
        ButterKnife.bind(this, root);
        setupToolbar();
        showInitialData();
        if (savedInstanceState == null) {
            addListingFragment();
        }
        return root;
    }

    private void addListingFragment() {
        DrinkListingFragment fragment = DrinkListingFragment.newInstance(ingredient.getName());
        getChildFragmentManager().beginTransaction()
                .add(R.id.ingredient_details_frag_container, fragment)
                .commitAllowingStateLoss();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BaseApplication) getActivity().getApplication()).getAppComponent()
                .plus(new IngredientDetailsModule())
                .inject(this);
        presenter.setView(this);
        presenter.loadData(ingredient.getName());

    }

    @Override
    public void showData(IngredientDetail ingredientDetail) {
//        descriptionTX.setText(ingredientDetail.getStrDescription());
    }

    private void showInitialData() {
//        ingredientNameTX.setText(ingredient.getName());
        Picasso.with(getContext()).load(ingredient.getLargeImagePath())
                .into(ingredientImg);
    }

    private void setupToolbar() {

        collapsingToolbar.setContentScrimColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        collapsingToolbar.setTitle(ingredient.getName());
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedToolbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedToolbarVisible);
        collapsingToolbar.setTitleEnabled(true);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

}
