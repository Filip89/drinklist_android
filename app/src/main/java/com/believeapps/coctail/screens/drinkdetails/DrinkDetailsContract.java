package com.believeapps.coctail.screens.drinkdetails;

import com.believeapps.coctail.screens.common.BasePresenter;
import com.believeapps.coctail.screens.common.BaseView;
import com.believeapps.coctail.data.model.DrinkDetail;

public interface DrinkDetailsContract {


    interface View extends BaseView<DrinkDetailsContract.Presenter> {

        void showDrinkDetail(DrinkDetail drinkDetail);
    }

    interface Presenter extends BasePresenter<DrinkDetailsContract.View> {
        void loadDrinkDetails(String drinkId);

    }


}
