package com.believeapps.coctail.screens.common;

import android.support.annotation.LayoutRes;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.believeapps.coctail.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrawerActivity extends AppCompatActivity {

    @BindView(R.id.drawer_layout)
    protected DrawerLayout drawerLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.navigation_view)
    protected NavigationView navigationView;

    protected FrameLayout frameLayout;


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(R.layout.activity_drawer);

        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.content_container);
        LayoutInflater.from(this).inflate(layoutResID, viewGroup, true);
        bindViews();
        setupToolbar();
    }

    private void bindViews() {
        ButterKnife.bind(this);
    }


    protected void setupToolbar() {
        if (toolbar != null) {
            toolbar.setTitle("Drink list");
            toolbar.setNavigationIcon(R.drawable.ic_menu_white);
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(view -> drawerLayout.openDrawer(Gravity.LEFT));
        }
    }

}
