package com.believeapps.coctail.screens.ingredientlisting;

import android.util.Log;

import com.believeapps.coctail.data.DataSource;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class IgredientListPresenter implements IngredientListContract.Presenter {

    private static final String TAG = "IgredientListPresenter";

    private DataSource dataSource;

    private IngredientListContract.View view;

    public IgredientListPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void setView(IngredientListContract.View view) {
        this.view = view;
        Log.d(TAG, "setView: view set");
    }

    @Override
    public void loadData() {
        view.startLoading();
        dataSource.getIngredients()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        ingredients -> {
                            view.finishLoading();
                            view.showData(ingredients);
                        }
                );
    }
}
