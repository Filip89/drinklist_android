package com.believeapps.coctail.screens.drinkdetails;


import dagger.Subcomponent;

@DrinkDetailsScope
@Subcomponent(modules = {DrinkDetailsModule.class})
public interface DrinkDetailsComponent {

    void inject(DrinkDetailsFragment target);

}
