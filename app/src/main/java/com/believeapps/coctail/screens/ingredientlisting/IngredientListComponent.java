package com.believeapps.coctail.screens.ingredientlisting;

import dagger.Subcomponent;

@IngredientListScope
@Subcomponent(modules = {IngredientListModule.class})
public interface IngredientListComponent {

    void inject(IngredientListFragment fragment);
}
