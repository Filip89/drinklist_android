package com.believeapps.coctail.screens.drinkdetails;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.believeapps.coctail.R;
import com.believeapps.coctail.screens.common.RootAdapter;
import com.believeapps.coctail.data.model.DrinkIngredient;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrinkIngredientAdapter extends RootAdapter<DrinkIngredient, DrinkIngredientAdapter.ViewHolder> {

    public DrinkIngredientAdapter(Context context) {
        super(context);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_ingredient_img_small)
        ImageView ingredientImg;
        @BindView(R.id.item_ingredient_name)
        TextView nameTX;
        @BindView(R.id.item_ingredient_measure)
        TextView measureTX;

        DrinkIngredient drinkIngredient;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, @SuppressLint("RecyclerView") DrinkIngredient drinkIngredient) {
        holder.drinkIngredient = drinkIngredient;
        holder.nameTX.setText(holder.drinkIngredient.getName());
        holder.measureTX.setText(holder.drinkIngredient.getMeasure());
        Picasso.with(context).load(holder.drinkIngredient.getSmallImagePath())
                .into(holder.ingredientImg);
    }


    @Override
    protected ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.details_ingriedient_item, parent, false);
        return new ViewHolder(itemView);
    }
}

