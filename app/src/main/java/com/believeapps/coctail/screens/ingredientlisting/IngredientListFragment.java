package com.believeapps.coctail.screens.ingredientlisting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.believeapps.coctail.R;
import com.believeapps.coctail.screens.common.RootAdapter;
import com.believeapps.coctail.data.model.Ingredient;
import com.believeapps.coctail.screens.igredientdetails.IngredientDetailsActivity;
import com.believeapps.coctail.root.BaseApplication;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IngredientListFragment extends Fragment implements IngredientListContract.View {


    private static final String TAG = "IngredientListFragment";

    public static final String INGREDIENT_ARRAY_LIST_KEY = "ingredient_array_list_key";

    private List<Ingredient> ingredients;
    private RootAdapter<Ingredient, IngredientAdapter.ViewHolder> adapter;

    @Inject
    IngredientListContract.Presenter presenter;

    @BindView(R.id.ingredient_list_recycler)
    RecyclerView recycler;

    @BindView(R.id.ingredient_list_progress)
    ProgressBar progressBar;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BaseApplication) getActivity().getApplication()).getAppComponent()
                .plus(new IngredientListModule())
                .inject(this);
        presenter.setView(this);

        initRecycler();

        if (savedInstanceState == null) {
            presenter.loadData();
        } else {
            if(savedInstanceState.containsKey(INGREDIENT_ARRAY_LIST_KEY)) {
                showData(savedInstanceState.getParcelableArrayList(INGREDIENT_ARRAY_LIST_KEY));
            }
        }

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.ingredient_list_fragment, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(ingredients instanceof ArrayList) {
            outState.putParcelableArrayList(INGREDIENT_ARRAY_LIST_KEY, (ArrayList<Ingredient>) ingredients);
        } else throw new ClassCastException("Only ArrayList can be put to bundle");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void showData(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
        adapter.swap(ingredients);
    }

    @Override
    public void startLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void finishLoading() {
        progressBar.setVisibility(View.GONE);
    }


    void initRecycler() {
        recycler.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapter = new IngredientAdapter(getContext());
        adapter.setOnItemClickListener(this::showIngredientDetailUI);
        recycler.setAdapter(adapter);
    }

    public void showIngredientDetailUI(Ingredient ingredient) {
        startActivity(IngredientDetailsActivity.newIntent(getContext(), ingredient));
    }
}
