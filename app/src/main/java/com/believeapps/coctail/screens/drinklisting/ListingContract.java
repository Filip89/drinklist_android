package com.believeapps.coctail.screens.drinklisting;

import com.believeapps.coctail.screens.common.BasePresenter;
import com.believeapps.coctail.screens.common.BaseView;
import com.believeapps.coctail.data.model.Drink;

import java.util.List;

public interface ListingContract {

    interface View extends BaseView<Presenter> {
        void showData(List<Drink> drinks);

        void startLoading();

        void finishLoading();

        void showErrorMsg();
    }

    interface Presenter extends BasePresenter<View> {
        void loadData(String ingredient);

    }


}
