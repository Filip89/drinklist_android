package com.believeapps.coctail.screens.ingredientlisting;

import com.believeapps.coctail.screens.common.BasePresenter;
import com.believeapps.coctail.screens.common.BaseView;
import com.believeapps.coctail.data.model.Ingredient;

import java.util.List;

public interface IngredientListContract {


    interface View extends BaseView<Presenter> {
        void showData(List<Ingredient> ingredients);

        void startLoading();

        void finishLoading();
    }

    interface Presenter extends BasePresenter<View> {
        void loadData();

    }



}
