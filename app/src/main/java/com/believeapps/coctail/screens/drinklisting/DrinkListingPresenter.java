package com.believeapps.coctail.screens.drinklisting;

import android.util.Log;

import com.believeapps.coctail.data.DataSource;
import com.believeapps.coctail.data.model.Drink;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@FragmentScope
public class DrinkListingPresenter implements ListingContract.Presenter {

    private static final String TAG = "DrinkListingPresenter";

    private ListingContract.View view;

    private DataSource dataSource;

    public DrinkListingPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void setView(ListingContract.View view) {
        this.view = view;
        Log.d(TAG, "setView: start");
    }

    @Override
    public void loadData(String ingredient) {
        view.startLoading();
        dataSource.getDrinksByIngredient(ingredient)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(drinks -> {
                    List<Drink> shuffledDrinks = new ArrayList<>(drinks);
                    Collections.shuffle(shuffledDrinks, new Random(System.nanoTime()));
                    return shuffledDrinks;
                })
                        .subscribe(
                                drinks -> {
                                    view.finishLoading();
                                    view.showData(drinks);
                                },
                                (err) -> {
                                    view.finishLoading();
                                    view.showErrorMsg();
                                }
                        );
    }
}
