package com.believeapps.coctail.screens.drinkdetails;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.believeapps.coctail.R;
import com.believeapps.coctail.data.model.Drink;

public class DrinkDetailsActivity extends AppCompatActivity {

    private static final String TAG = "DrinkDetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_details);

        if (savedInstanceState == null) {

            Bundle extras = getIntent().getExtras();
            if (extras != null && extras.containsKey(Drink.class.getSimpleName())) {

                Drink drink = extras.getParcelable(Drink.class.getSimpleName());
                DrinkDetailsFragment fragment = DrinkDetailsFragment.getInstance(drink);

                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.drink_details_container, fragment)
                        .commit();
            }
        }
    }


    public static Intent newIntent(Context packageContext, Drink drink) {
        Intent intent = new Intent(packageContext, DrinkDetailsActivity.class);
        Bundle extras = new Bundle();
        extras.putParcelable(Drink.class.getSimpleName(), drink);
        intent.putExtras(extras);
        return intent;
    }

}
