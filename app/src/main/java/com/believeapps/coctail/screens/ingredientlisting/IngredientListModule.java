package com.believeapps.coctail.screens.ingredientlisting;

import com.believeapps.coctail.data.DataSource;

import dagger.Module;
import dagger.Provides;

@Module
public class IngredientListModule {


    @Provides
    @IngredientListScope
    IngredientListContract.Presenter providePresenter(DataSource dataSource) {
        return new IgredientListPresenter(dataSource);
    }
}
