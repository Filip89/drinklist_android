package com.believeapps.coctail.screens.drinklisting;

import com.believeapps.coctail.data.DataSource;

import dagger.Module;
import dagger.Provides;

@Module
public class DrinkListingFragModule {

    @Provides
    @FragmentScope
    ListingContract.Presenter providePresenter(DataSource dataSource) {
        return new DrinkListingPresenter(dataSource);
    }

}
