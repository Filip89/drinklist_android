package com.believeapps.coctail.screens.igredientdetails;

import com.believeapps.coctail.data.DataSource;

import dagger.Module;
import dagger.Provides;

@Module
public class IngredientDetailsModule {

    @Provides
    IngredientDetailsContract.Presenter providePresenter(DataSource dataSource) {
        return new IngredientDetailsPresenter(dataSource);
    }
}
