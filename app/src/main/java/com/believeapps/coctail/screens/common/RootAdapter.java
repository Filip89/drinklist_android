package com.believeapps.coctail.screens.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class RootAdapter <T, VH extends RecyclerView.ViewHolder>
    extends RecyclerView.Adapter<RecyclerView.ViewHolder>  implements View.OnClickListener{

    private static final String TAG = "RootAdapter";

    public interface OnItemClickListener<T> {
        void onItemClick(T item);
    }

    private List<T> objects;

    protected final Context context;
    private final LayoutInflater inflater;
    private OnItemClickListener<T> itemClickListener;


    public RootAdapter(Context context) {
        this.context = context;
        objects = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    public void addAll(Collection<T> objects) {
        if (objects != null) {
            this.objects.addAll(objects);
            notifyDataSetChanged();
        } else {
            throw new NullPointerException("Passed objects collection cannot be null");
        }
    }

    public void swap(Collection<T> objects) {
        this.objects.clear();
        this.objects.addAll(objects);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener<T> listener) {
        itemClickListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = onCreateViewHolder(inflater, parent, viewType);
        if (itemClickListener != null) {
            holder.itemView.setOnClickListener(this);
            holder.itemView.setTag(holder);
        }
        return holder;
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    @Override
    public void onClick(View view) {
        VH holder = (VH) view.getTag();
        int position = holder.getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            itemClickListener.onItemClick(getItemFromAdapterPosition(position));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindViewHolder((VH)holder,  getItemFromAdapterPosition(position));
    }

    protected abstract void onBindViewHolder(VH holder, T item);

    public T getItemFromAdapterPosition(int position) {
        return objects.get(position);
    }

    public T getItem(int position) {
        return objects.get(position);
    }

    protected abstract VH onCreateViewHolder(LayoutInflater inflater, ViewGroup parent,
                                             int viewType);
}
