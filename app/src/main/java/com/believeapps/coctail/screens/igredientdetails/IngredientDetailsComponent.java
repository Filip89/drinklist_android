package com.believeapps.coctail.screens.igredientdetails;

import dagger.Subcomponent;

@Subcomponent(modules = {IngredientDetailsModule.class})
public interface IngredientDetailsComponent {

    void inject(IngredientDetailsFragment target);

}
