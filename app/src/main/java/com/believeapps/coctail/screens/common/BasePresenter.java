package com.believeapps.coctail.screens.common;

public interface BasePresenter<T> {

    void setView(T view);
}
