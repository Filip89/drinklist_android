package com.believeapps.coctail.screens.drinklisting;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.believeapps.coctail.R;
import com.believeapps.coctail.screens.common.RootAdapter;
import com.believeapps.coctail.data.model.Drink;
import com.believeapps.coctail.screens.drinkdetails.DrinkDetailsActivity;
import com.believeapps.coctail.root.BaseApplication;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrinkListingFragment extends Fragment implements ListingContract.View {

    private static final String TAG = "DrinkListingFragment";

    private static final String ARG_INGREDIENT = "section_number";

    public static final String DRINKS_ARRAY_LIST_KEY = "drinks_array_list_key";


    @Inject
    ListingContract.Presenter presenter;

    private RootAdapter<Drink, DrinkAdapter.ViewHolder> adapter;

    private List<Drink> drinks = new ArrayList<>();

    /**
     * Defines what type of drink to show
     */
    private String ingredient;


    @BindView(R.id.drink_listing_recycler)
    RecyclerView drinksRecycler;

    @BindView(R.id.drink_listing_progress_circle)
    ProgressBar progressCircle;

    @BindView(R.id.drink_listing_error_tx)
    TextView errorMsg;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ingredient = getArguments().getString(ARG_INGREDIENT);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //save array list to bundle to retrieve it when fragment is recreated so there is no need
        //to shoot again to api
        if(drinks instanceof ArrayList) {
            outState.putParcelableArrayList(DRINKS_ARRAY_LIST_KEY, (ArrayList<Drink>) drinks);
        } else throw new ClassCastException("Only ArrayList can be put to bundle");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((BaseApplication) getActivity().getApplication()).getAppComponent()
                .plus(new DrinkListingFragModule())
                .inject(this);
        presenter.setView(this);

        initRecycler();

        //create list and load data from data source if there was no previously saved list
        if (savedInstanceState == null) {
            presenter.loadData(ingredient);
        } else {
           showData(savedInstanceState.getParcelableArrayList(DRINKS_ARRAY_LIST_KEY));
        }


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void showDrinkDetailUi(Drink drink) {
        startActivity(DrinkDetailsActivity.newIntent(getContext(), drink));
    }


    private void initRecycler() {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        drinksRecycler.setLayoutManager(layoutManager);
        adapter = new DrinkAdapter(getContext());
        adapter.setOnItemClickListener(this::showDrinkDetailUi);
        drinksRecycler.setAdapter(adapter);
    }


    @Override
    public void showData(List<Drink> drinks) {
        this.drinks = drinks;
        adapter.swap(drinks);
    }

    @Override
    public void startLoading() {
        errorMsg.setVisibility(View.GONE);
        progressCircle.setVisibility(View.VISIBLE);
    }

    @Override
    public void finishLoading() {
        progressCircle.setVisibility(View.GONE);

    }

    @Override
    public void showErrorMsg() {
        errorMsg.setVisibility(View.VISIBLE);
    }

    public static DrinkListingFragment newInstance(String ingredient) {
        DrinkListingFragment fragment = new DrinkListingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_INGREDIENT, ingredient);
        fragment.setArguments(args);
        return fragment;
    }
}
