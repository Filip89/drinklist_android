package com.believeapps.coctail.screens.igredientdetails;

import com.believeapps.coctail.screens.common.BasePresenter;
import com.believeapps.coctail.screens.common.BaseView;
import com.believeapps.coctail.data.model.IngredientDetail;

public interface IngredientDetailsContract {

    interface View extends BaseView<Presenter> {
        void showData(IngredientDetail ingredientDetail);
    }


    interface Presenter extends BasePresenter<View> {

        void loadData(String ingredientName);
    }


}
