package com.believeapps.coctail.screens.drinkdetails;

import com.believeapps.coctail.data.DataSource;

import dagger.Module;
import dagger.Provides;

@Module
public class DrinkDetailsModule {


    @Provides
    @DrinkDetailsScope
    DrinkDetailsContract.Presenter providePresenter(DataSource dataSource) {
        return new DrinkDetailsPresenter(dataSource);
    }
}
