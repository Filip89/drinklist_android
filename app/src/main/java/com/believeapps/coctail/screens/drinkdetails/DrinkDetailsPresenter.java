package com.believeapps.coctail.screens.drinkdetails;

import com.believeapps.coctail.data.DataSource;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DrinkDetailsPresenter implements DrinkDetailsContract.Presenter {

    private static final String TAG = "DrinkDetailsPresenter";


    private DataSource dataSource;
    private DrinkDetailsContract.View view;

    public DrinkDetailsPresenter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void setView(DrinkDetailsContract.View view) {
        this.view = view;

    }

    @Override
    public void loadDrinkDetails(String drinkId) {
        dataSource.getDrinksDetailById(drinkId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        drinkDetail -> {
                            view.showDrinkDetail(drinkDetail);
                        }
                );
    }



}
