package com.believeapps.coctail.root;


import com.believeapps.coctail.screens.drinkdetails.DrinkDetailsComponent;
import com.believeapps.coctail.screens.drinkdetails.DrinkDetailsModule;
import com.believeapps.coctail.screens.drinklisting.DrinkListingComponent;
import com.believeapps.coctail.screens.drinklisting.DrinkListingFragModule;
import com.believeapps.coctail.screens.igredientdetails.IngredientDetailsComponent;
import com.believeapps.coctail.screens.igredientdetails.IngredientDetailsModule;
import com.believeapps.coctail.screens.ingredientlisting.IngredientListComponent;
import com.believeapps.coctail.screens.ingredientlisting.IngredientListModule;
import com.believeapps.coctail.root.network.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class
})
public interface AppComponent {

    DrinkListingComponent plus(DrinkListingFragModule module);

    DrinkDetailsComponent plus(DrinkDetailsModule module);

    IngredientListComponent plus(IngredientListModule module);

    IngredientDetailsComponent plus(IngredientDetailsModule module);
}
