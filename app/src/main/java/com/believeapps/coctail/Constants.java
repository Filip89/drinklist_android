package com.believeapps.coctail;

public class Constants {

    public static final String BASE_URL_VALUE = "http://www.thecocktaildb.com/api/json/v1/1/";

    public static final String INGREDIENT_IMAGE_BASE_PATH = "http://www.thecocktaildb.com/images/ingredients/";

}
