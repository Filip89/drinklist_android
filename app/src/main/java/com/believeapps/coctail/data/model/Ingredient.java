package com.believeapps.coctail.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.believeapps.coctail.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ingredient implements Parcelable {

    private static final String SMALL_SUFFIX = "-Small.png";
    private static final String MEDIUM_SUFFIX = "-Medium.png";
    private static final String LARGE_SUFFIX = ".png";


    @SerializedName("strIngredient1")
    @Expose
    private String name;


    public final static Parcelable.Creator<Ingredient> CREATOR = new Creator<Ingredient>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        public Ingredient[] newArray(int size) {
            return (new Ingredient[size]);
        }

    };

    protected Ingredient(Parcel in) {
        name = ((String) in.readValue((String.class.getClassLoader())));
    }


    public Ingredient(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getSmallImagePath() {
        return Constants.INGREDIENT_IMAGE_BASE_PATH + replaceSpace(name) + SMALL_SUFFIX;
    }

    public String getMediumImagePath() {
        return Constants.INGREDIENT_IMAGE_BASE_PATH + replaceSpace(name) + MEDIUM_SUFFIX;
    }

    public String getLargeImagePath() {
        return Constants.INGREDIENT_IMAGE_BASE_PATH + replaceSpace(name) + LARGE_SUFFIX;
    }

    /**
     *replaces space with %20 so name confines to web URL standard
     */
    private String replaceSpace(String string) {
        if(string == null) return null;
        return string.replace(" ", "%20");
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(name);
    }
}

