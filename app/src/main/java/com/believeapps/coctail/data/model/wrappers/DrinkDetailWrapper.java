package com.believeapps.coctail.data.model.wrappers;

import com.believeapps.coctail.data.model.DrinkDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DrinkDetailWrapper {

    @SerializedName("drinks")
    @Expose
    private List<DrinkDetail> drinks = null;

    public List<DrinkDetail> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<DrinkDetail> drinks) {
        this.drinks = drinks;
    }
}
