package com.believeapps.coctail.data.api;

import com.believeapps.coctail.data.model.Ingredient;
import com.believeapps.coctail.data.model.wrappers.IngredientDetailWrapper;
import com.believeapps.coctail.data.model.wrappers.IngredientListWrapper;
import com.believeapps.coctail.data.model.wrappers.DrinkDetailWrapper;
import com.believeapps.coctail.data.model.wrappers.DrinkWrapper;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DrinksApi {


    @GET("filter.php")
    Observable<DrinkWrapper> getDrinksByIngredients(@Query("i") String ingredient);

    @GET("lookup.php?")
    Observable<DrinkDetailWrapper> getDrinkDetail(@Query("i") String idDrink);

    @GET("list.php?i=list")
    Observable<IngredientListWrapper> getAllIngredients();

    @GET("search.php?")
    Observable<IngredientDetailWrapper> getIngredientDetailsByName(@Query("i") String ingredientName);

}
