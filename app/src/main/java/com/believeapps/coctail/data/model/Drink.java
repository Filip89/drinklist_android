package com.believeapps.coctail.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Drink implements Parcelable
{

    @SerializedName("strDrink")
    @Expose
    private String strDrink;
    @SerializedName("strDrinkThumb")
    @Expose
    private String strDrinkThumb;
    @SerializedName("idDrink")
    @Expose
    private String idDrink;
    public final static Parcelable.Creator<Drink> CREATOR = new Creator<Drink>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Drink createFromParcel(Parcel in) {
            return new Drink(in);
        }

        public Drink[] newArray(int size) {
            return (new Drink[size]);
        }

    };

    protected Drink(Parcel in) {
        this.strDrink = ((String) in.readValue((String.class.getClassLoader())));
        this.strDrinkThumb = ((String) in.readValue((String.class.getClassLoader())));
        this.idDrink = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Drink() {
    }

    public String getStrDrink() {
        return strDrink;
    }

    public void setStrDrink(String strDrink) {
        this.strDrink = strDrink;
    }

    public String getStrDrinkThumb() {
        return strDrinkThumb;
    }

    public void setStrDrinkThumb(String strDrinkThumb) {
        this.strDrinkThumb = strDrinkThumb;
    }

    public String getIdDrink() {
        return idDrink;
    }

    public void setIdDrink(String idDrink) {
        this.idDrink = idDrink;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(strDrink);
        dest.writeValue(strDrinkThumb);
        dest.writeValue(idDrink);
    }

    public int describeContents() {
        return 0;
    }

}