package com.believeapps.coctail.data;

import com.believeapps.coctail.data.model.Drink;
import com.believeapps.coctail.data.model.DrinkDetail;
import com.believeapps.coctail.data.model.Ingredient;
import com.believeapps.coctail.data.model.IngredientDetail;

import java.util.List;

import io.reactivex.Observable;

public interface DataSource {

    Observable<List<Drink>> getDrinksByIngredient(String ingrident);

    Observable<DrinkDetail> getDrinksDetailById(String idDrink);

    Observable<List<Ingredient>> getIngredients();

    Observable<IngredientDetail> getIngredientDetail(String ingredientName);

}
