package com.believeapps.coctail.data.model.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

import com.believeapps.coctail.data.model.IngredientDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IngredientDetailWrapper {

    @SerializedName("ingredients")
    @Expose
    private List<IngredientDetail> IngredientDetails = null;
    public final static Parcelable.Creator<IngredientDetailWrapper> CREATOR = new Parcelable.Creator<IngredientDetailWrapper>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IngredientDetailWrapper createFromParcel(Parcel in) {
            return new IngredientDetailWrapper(in);
        }

        public IngredientDetailWrapper[] newArray(int size) {
            return (new IngredientDetailWrapper[size]);
        }

    };

    protected IngredientDetailWrapper(Parcel in) {
        in.readList(this.IngredientDetails, (IngredientDetail.class.getClassLoader()));
    }

    public IngredientDetailWrapper() {
    }

    public List<IngredientDetail> getIngredientDetails() {
        return IngredientDetails;
    }

    public void setIngredientDetails(List<IngredientDetail> IngredientDetails) {
        this.IngredientDetails = IngredientDetails;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(IngredientDetails);
    }

    public int describeContents() {
        return 0;
    }

    
}
