package com.believeapps.coctail.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IngredientDetail {

    @SerializedName("idIngredient")
    @Expose
    private String idIngredient;
    @SerializedName("strIngredient")
    @Expose
    private String strIngredient;
    @SerializedName("strDescription")
    @Expose
    private String strDescription;
    @SerializedName("strType")
    @Expose
    private String strType;
    public final static Parcelable.Creator<IngredientDetail> CREATOR = new Parcelable.Creator<IngredientDetail>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IngredientDetail createFromParcel(Parcel in) {
            return new IngredientDetail(in);
        }

        public IngredientDetail[] newArray(int size) {
            return (new IngredientDetail[size]);
        }

    };

    protected IngredientDetail(Parcel in) {
        this.idIngredient = ((String) in.readValue((String.class.getClassLoader())));
        this.strIngredient = ((String) in.readValue((String.class.getClassLoader())));
        this.strDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.strType = ((String) in.readValue((String.class.getClassLoader())));
    }

    public IngredientDetail() {
    }

    public String getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(String idIngredient) {
        this.idIngredient = idIngredient;
    }

    public String getStrIngredient() {
        return strIngredient;
    }

    public void setStrIngredient(String strIngredient) {
        this.strIngredient = strIngredient;
    }

    public String getStrDescription() {
        return strDescription;
    }

    public void setStrDescription(String strDescription) {
        this.strDescription = strDescription;
    }

    public String getStrType() {
        return strType;
    }

    public void setStrType(String strType) {
        this.strType = strType;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idIngredient);
        dest.writeValue(strIngredient);
        dest.writeValue(strDescription);
        dest.writeValue(strType);
    }

    public int describeContents() {
        return 0;
    }
}
