package com.believeapps.coctail.data;

import com.believeapps.coctail.data.api.DrinksApi;
import com.believeapps.coctail.data.model.Drink;
import com.believeapps.coctail.data.model.DrinkDetail;
import com.believeapps.coctail.data.model.Ingredient;
import com.believeapps.coctail.data.model.IngredientDetail;
import com.believeapps.coctail.data.model.wrappers.DrinkWrapper;
import com.believeapps.coctail.data.model.wrappers.IngredientListWrapper;

import java.util.List;

import io.reactivex.Observable;

public class DataSourceImpl implements DataSource {

    private DrinksApi api;

    public DataSourceImpl(DrinksApi api) {
        this.api = api;
    }

    @Override
    public Observable<List<Drink>> getDrinksByIngredient(String ingredient) {
        return api.getDrinksByIngredients(ingredient).map(DrinkWrapper::getDrinks);
    }


    @Override
    public Observable<DrinkDetail> getDrinksDetailById(String idDrink) {
        return api.getDrinkDetail(idDrink).map(drinkDetailWrapper -> drinkDetailWrapper.getDrinks().get(0));
    }

    @Override
    public Observable<List<Ingredient>> getIngredients() {
        return api.getAllIngredients().map(IngredientListWrapper::getIngredients);
    }

    @Override
    public Observable<IngredientDetail> getIngredientDetail(String ingredientName) {
        return api.getIngredientDetailsByName(ingredientName).map(wrapper -> wrapper.getIngredientDetails().get(0));
    }
}
