package com.believeapps.coctail.data.model.wrappers;

import com.believeapps.coctail.data.model.Ingredient;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IngredientListWrapper {

    @SerializedName("drinks")
    @Expose
    private List<Ingredient> ingredients = null;

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setDrinks(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
