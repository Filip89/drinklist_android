package com.believeapps.coctail.data.model;

import com.believeapps.coctail.Constants;

public class DrinkIngredient extends Ingredient {


    public DrinkIngredient(String name, String measure) {
        super(name);
        this.measure = measure;
    }

    private final String measure;


    public String getMeasure() {
        return measure;
    }


}
